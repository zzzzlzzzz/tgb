-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema exchange_db
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `exchange_db` ;

-- -----------------------------------------------------
-- Schema exchange_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `exchange_db` DEFAULT CHARACTER SET utf8 ;
USE `exchange_db` ;

-- -----------------------------------------------------
-- Table `exchange_db`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `exchange_db`.`user` ;

CREATE TABLE IF NOT EXISTS `exchange_db`.`user` (
  `name` VARCHAR(32) NOT NULL,
  `state` VARCHAR(256) NULL,
  `information` BLOB NULL,
  PRIMARY KEY (`name`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `exchange_db`.`query`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `exchange_db`.`query` ;

CREATE TABLE IF NOT EXISTS `exchange_db`.`query` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(32) NOT NULL,
  `date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `information` BLOB NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_query_user_idx` (`user_name` ASC),
  CONSTRAINT `fk_query_user`
    FOREIGN KEY (`user_name`)
    REFERENCES `exchange_db`.`user` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
