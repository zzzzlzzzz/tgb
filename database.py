import json
import pymysql
import DBUtils.PooledDB


class Database:
    """
    Класс служит для взаимодействия с базой данных
    """
    def __init__(self, connection_pool):
        """
        Начальная инициализация
        :param connection_pool: пул соединений с БД
        """
        self.connection_pool = connection_pool

    def __enter__(self):
        """
        Для обработки инструкции with
        :return:
        """
        self.connection = self.connection_pool.connection()
        self.cursor = self.connection.cursor()
        return self.cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Для обработки инструкции with
        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return:
        """
        self.connection.commit()
        self.cursor.close()
        self.connection.close()

    @staticmethod
    def get_connection_pool(config: 'dict'):
        """
        Создает пул соединений с БД
        :param config:
        :return:
        """
        return DBUtils.PooledDB.PooledDB(creator=pymysql, **config)

    @staticmethod
    def get_user_state(cursor, name):
        """
        Получает состояние пользователя из БД
        :param cursor: курсор БД
        :param name: имя пользователя
        :return: строка состояния, None иначе
        """
        _SQL = 'SELECT `state` FROM `user` WHERE `name` = %s'
        cursor.execute(_SQL, (name,))
        row = cursor.fetchone()
        if row:
            return row[0]
        return None

    @staticmethod
    def set_user_state(cursor, name, state):
        """
        Устанавливает состояние пользователя в БД
        :param cursor: курсор БД
        :param name: имя пользователя
        :param state: строка состояния пользователя
        :return: None
        """
        _SQL = 'UPDATE `user` SET `state` = %s WHERE `name` = %s'
        cursor.execute(_SQL, (state, name))

    @staticmethod
    def make_user(cursor, name, state):
        """
        Вызывается при появлении нового пользователя
        :param cursor:
        :param name:
        :param state:
        :return:
        """
        _SQL = 'INSERT INTO `user` (`name`,`state`) VALUES (%s, %s)'
        cursor.execute(_SQL, (name, state))

    @staticmethod
    def set_user_information(cursor, name: 'str', information: 'dict'):
        """
        Устанавливает столбец информации для временного пользования
        :param cursor: курсор БД
        :param name: имя пользователя
        :param information: словарь с информацией о пользователе
        :return:
        """
        _SQL = 'UPDATE `user` SET `information` = %s WHERE `name` = %s'
        cursor.execute(_SQL, (json.dumps(information), name))

    @staticmethod
    def get_user_information(cursor, name: 'str') -> 'dict':
        """
        Получает информацию для временного пользования о пользователе
        :param cursor:
        :param name:
        :return: Словарь с информацией о пользователе, None иначе
        """
        _SQL = 'SELECT `information` FROM `user` WHERE `name` = %s'
        cursor.execute(_SQL, (name,))
        row = cursor.fetchone()
        if row:
            return json.loads(row[0])
        return None

    @staticmethod
    def make_wait_query(cursor, name: 'str', information: 'dict') -> None:
        """
        Создает ожидающий запрос в БД (используется для оплаты поступающих транзакций)
        :param cursor: курсор БД
        :param name: имя пользователя, для кого создается транзакция
        :param information: словарь с информацией о транзакции
        :return: None
        """
        _SQL = 'INSERT INTO `query` (`user_name`, `information`) VALUES (%s, %s)'
        cursor.execute(_SQL, (name, json.dumps(information)))

    @staticmethod
    def get_wait_query(cursor, name: 'str' = None) -> list:
        """
        Возвращает список с запросами
        :param cursor: курсор БД
        :param name: имя пользователя, чьи запросы следует узнать (None - любого пользователя)
        :return: список, содержащий ожидающие запросы
        """
        if name:
            _SQL = 'SELECT `information` FROM `query` WHERE `user_name` = %s'
            cursor.execute(_SQL, (name, ))
        else:
            _SQL = 'SELECT `information` FROM `query`'
            cursor.execute(_SQL)

        query_list = list()
        row = cursor.fetchone()
        while row:
            query_list.append(row[0])
            row = cursor.fetchone()
        return query_list

    @staticmethod
    def remove_old_query(cursor, time_to_live: 'int') -> None:
        """
        Выполняет удаление старых записей запросов
        :param cursor: курсор БД
        :param time_to_live: время в часах, по истечению которого записи будут удалены
        :return: None
        """
        _SQL = 'DELETE FROM `query` WHERE NOW() > (`date` + INTERVAL %s HOUR) AND `id` > 0'
        cursor.execute(_SQL, (time_to_live, ))
