class BGWorker:
    def __init__(self):
        """
        Выполняет инициализацию бэкграунд-рабочего
        """
        pass

    def execute(self, bot, db_cursor) -> None:
        """
        Исполняет задачи в очереди (обработка и выплата заявок)
        :param bot: Telegram бот, от имени которого работаем
        :param db_cursor: Курсор для работы с БД
        :return: None
        """
        pass
