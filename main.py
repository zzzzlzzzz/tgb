import telegram
import telegram.ext
import telegram.ext.messagequeue
import telegram.ext.updater
import telegram.utils.request

import database
import bgworker


class ExchangerBot:
    class MQBot(telegram.bot.Bot):
        def __init__(self, *args, is_queued_def=True, mqueue=None, **kwargs):
            super().__init__(*args, **kwargs)
            self._is_messages_queued_default = is_queued_def
            self._msg_queue = mqueue or telegram.ext.messagequeue.MessageQueue()

        def __del__(self):
            try:
                self._msg_queue.stop()
            except:
                pass
            if hasattr(super(), '__del__'):
                super().__del__()

        @telegram.ext.messagequeue.queuedmessage
        def send_message(self, *args, **kwargs):
            return super().send_message(*args, **kwargs)

    def __init__(self, configuration: 'dict'):
        """
        Выполняет инициализацию бота-обменника
        :param configuration: конфигурация бота
        """
        self.configuration = configuration
        self.db_connection_pool = database.Database.get_connection_pool(self.configuration['database'])
        self.background_worker = bgworker.BGWorker()

    def start(self) -> 'None':
        """
        Запускает бот-обменник с заданной конфигурацией
        :return: None
        """
        updater = telegram.ext.updater.Updater(
            bot=ExchangerBot.MQBot(self.configuration['token'],
                                   request=telegram.utils.request.Request(**(self.configuration['request_options'])),
                                   mqueue=telegram.ext.messagequeue.MessageQueue(**(self.configuration['message_queue_options']))))

        updater.dispatcher.add_handler(telegram.ext.CallbackQueryHandler(self.on_button_callback))
        updater.dispatcher.add_handler(telegram.ext.CommandHandler('start', self.on_start))
        updater.dispatcher.add_handler(telegram.ext.CommandHandler('help', self.on_help))
        updater.dispatcher.add_handler(telegram.ext.MessageHandler(telegram.ext.Filters.text, self.on_message_receive))

        updater.job_queue.run_repeating(self.on_background_work,
                                        interval=self.configuration['bg_repeat_interval'],
                                        first=0)

        """
            Следует заменить на webpool, когда будет работать на vps
            (однако, если нагрузка будет небольшая, можно и оставить)
        """
        updater.start_polling()
        updater.idle()

    def on_background_work(self, bot, job: 'telegram.ext.job') -> None:
        """
        Функция вызывается через определенные интервалы времени для обработки заявок, получаемых от пользователей
        :param bot: Telegram бот, от имени которого работаем
        :param job: Идентификатор работы
        :return: None
        """
        with database.Database(self.db_connection_pool) as db_cursor:
            self.background_worker.execute(bot, db_cursor)

    def on_message_receive(self, bot, update: 'telegram.Update'):
        main_menu_markup = telegram.InlineKeyboardMarkup(
            [
                [telegram.InlineKeyboardButton('Обменять', callback_data='main_menu_exchange')],
                [telegram.InlineKeyboardButton('Курсы и лимиты', callback_data='main_menu_exchange_status')],
                [telegram.InlineKeyboardButton('Техподдержка', callback_data='main_menu_support')]
            ])

        if update.effective_user is None:
            return

        with database.Database(self.db_connection_pool) as db_cursor:
            cur_user_state = database.Database.get_user_state(db_cursor, update.effective_user.username)
            if cur_user_state is None:
                cur_user_state = 'main_menu'
                database.Database.make_user(db_cursor, update.effective_user.username, cur_user_state)

            if cur_user_state == 'main_menu':
                bot.send_message(chat_id=update.message.chat_id, text='Что изволите?', reply_markup=main_menu_markup)
            elif cur_user_state == 'exchange_btcqiwi_step_1':
                cur_user_state = 'exchange_btcqiwi_step_2'

                user_info = database.Database.get_user_information(db_cursor, update.effective_user.username)
                user_info['amount'] = float(update.message.text)
                database.Database.set_user_information(db_cursor, update.effective_user.username, user_info)

                bot.send_message(chat_id=update.message.chat_id, text='Введите QIWI кошелек (прим.: +79522455623):')
            elif cur_user_state == 'exchange_btcqiwi_step_2':

                user_info = database.Database.get_user_information(db_cursor, update.effective_user.username)
                user_info['user_wallet'] = update.message.text
                database.Database.set_user_information(db_cursor, update.effective_user.username, user_info)

                bot.send_message(chat_id=update.message.chat_id, text='Вы заказываете обмен {0} BTC\n'
                                                                      'Вы получите P QIWI на ({1} QIWI)\n'
                                                                      'Отправьте {0} BTC на кошелек '
                                                                      '(нужен API BTC)'.format(user_info['amount'],
                                                                                               user_info['user_wallet']))

            database.Database.set_user_state(db_cursor, update.effective_user.username, cur_user_state)

    def on_start(self, bot, update: 'telegram.Update'):
        """
        Вызывается при получении команды /start
        :param bot:
        :param update:
        :return:
        """
        main_menu_markup = telegram.InlineKeyboardMarkup(
            [
                [telegram.InlineKeyboardButton('Обменять', callback_data='main_menu_exchange')],
                [telegram.InlineKeyboardButton('Курсы и лимиты', callback_data='main_menu_exchange_status')],
                [telegram.InlineKeyboardButton('Техподдержка', callback_data='main_menu_support')]
            ])

        if update.effective_user is None:
            return

        with database.Database(self.db_connection_pool) as db_cursor:
            cur_user_state = database.Database.get_user_state(db_cursor, update.effective_user.username)
            if cur_user_state is None:
                cur_user_state = 'main_menu'
                database.Database.make_user(db_cursor, update.effective_user.username, cur_user_state)

            if cur_user_state == 'main_menu':
                bot.send_message(chat_id=update.message.chat_id, text='Что изволите?', reply_markup=main_menu_markup)

            database.Database.set_user_state(db_cursor, update.effective_user.username, cur_user_state)

    def on_help(self, bot, update: 'telegram.Update'):
        """
        Вызывается при получении команды /help
        :param bot:
        :param update:
        :return:
        """
        bot.send_message(chat_id=update.message.chat_id,
                         text='*Для чего этот бот*?\n'
                              'Бот предназначен для обмена электронных валют\n\n'
                              '*Как начать пользоваться ботом?*\n'
                              '1. Используйте команду /start\n'
                              '2. Следуйте подсказкам бота\n\n'
                              '*Что насчет вопросов, предложений и претензий?*\n'
                              'Сообщите нашей техподдержке (в главном меню)\n',
                         parse_mode=telegram.ParseMode.MARKDOWN)

    def on_button_callback(self, bot, update: 'telegram.Update'):
        """
        Вызывается при нажатии кнопки
        :param bot:
        :param update:
        :return:
        """
        main_menu_markup = telegram.InlineKeyboardMarkup(
            [
                [telegram.InlineKeyboardButton('Обменять', callback_data='main_menu_exchange')],
                [telegram.InlineKeyboardButton('Курсы и лимиты', callback_data='main_menu_exchange_status')],
                [telegram.InlineKeyboardButton('Техподдержка', callback_data='main_menu_support')]
            ])

        exchange_direction_menu_markup = telegram.InlineKeyboardMarkup(
            [
                [
                    telegram.InlineKeyboardButton('BTC -> QIWI', callback_data='exchange_direction_menu_btcqiwi'),
                    telegram.InlineKeyboardButton('QIWI -> BTC', callback_data='exchange_direction_menu_qiwibtc')
                ],
                [
                    telegram.InlineKeyboardButton('Назад', callback_data='exchange_direction_menu_back')
                ]
            ])
        exchange_status_menu_markup = telegram.InlineKeyboardMarkup(
            [
                [telegram.InlineKeyboardButton('Назад', callback_data='exchange_status_menu_back')],
            ])
        support_menu_markup = telegram.InlineKeyboardMarkup(
            [
                [telegram.InlineKeyboardButton('Назад', callback_data='support_menu_back')],
            ])

        query = update.callback_query

        if update.effective_user is None:
            return

        with database.Database(self.db_connection_pool) as db_cursor:
            cur_user_state = database.Database.get_user_state(db_cursor, update.effective_user.username)
            if cur_user_state is None:
                cur_user_state = 'main_menu'
                database.Database.make_user(db_cursor, update.effective_user.username, cur_user_state)

            if cur_user_state == 'main_menu':  # Обрабатываем пункты главного меню
                if query.data == 'main_menu_exchange':
                    cur_user_state = 'exchange_direction_menu'
                    query.edit_message_text(text='Выберите желаемое направление обмена',
                                            reply_markup=exchange_direction_menu_markup)
                elif query.data == 'main_menu_exchange_status':
                    cur_user_state = 'exchange_status_menu'
                    query.edit_message_text(text='1 BTC -> 1000000 QIWI (доступно 100 QIWI)\n'
                                                 '1000000 QIWI -> 1 BTC (доступно 3 BTC)\n'
                                                 'Мало? Попроси пополнить у @zzzzlzzzzz',
                                            reply_markup=exchange_status_menu_markup)
                elif query.data == 'main_menu_support':
                    cur_user_state = 'support_menu'
                    query.edit_message_text(text='Спрашивайте всё у @zzzzlzzzz',
                                            reply_markup=support_menu_markup)
            elif cur_user_state == 'exchange_direction_menu':  # Обрабатываем пункты меню выбора направления обмена
                if query.data == 'exchange_direction_menu_btcqiwi':
                    cur_user_state = 'exchange_btcqiwi_step_1'
                    database.Database.set_user_information(db_cursor, update.effective_user.username,
                                                           {
                                                               'operation': 'exchange',
                                                               'pair': 'btcqiwi'
                                                           })
                    """
                        {
                            'operation': 'exchange',
                            'pair': 'btcqiwi',
                            'amount': 2.5
                            'user_wallet': '+7954458874'
                        }
                    """
                    query.edit_message_text(text='Введите количество BTC, которое вы отдаете (прим.: 2.25):')
                elif query.data == 'exchange_direction_menu_qiwibtc':
                    cur_user_state = 'exchange_qiwibtc'
                elif query.data == 'exchange_direction_menu_back':
                    cur_user_state = 'main_menu'
                    query.edit_message_text(text='Что изволите?',
                                            reply_markup=main_menu_markup)
            elif cur_user_state == 'exchange_status_menu':  # Обрабатываем пункты меню информации о курсах
                if query.data == 'exchange_status_menu_back':
                    cur_user_state = 'main_menu'
                    query.edit_message_text(text='Что изволите?',
                                            reply_markup=main_menu_markup)
            elif cur_user_state == 'support_menu':  # Обрабатываем пункты меню поддержки
                if query.data == 'support_menu_back':
                    cur_user_state = 'main_menu'
                    query.edit_message_text(text='Что изволите?',
                                            reply_markup=main_menu_markup)

            database.Database.set_user_state(db_cursor, update.effective_user.username, cur_user_state)

if __name__ == '__main__':
    config = {
        'token': '699393002:AAFv6FNeXqO7IEJSAR3UHTnj4iH9xdcmnkk',
        'message_queue_options': {
            'all_burst_limit': 29,
            'all_time_limit_ms': 1017
        },
        'request_options': {
            'con_pool_size': 8,
            'proxy_url': 'https://66.70.255.195:3128'
        },
        'database': {
            'host': 'localhost',
            'user': 'exchange_bot',
            'passwd': 'exchange_bot',
            'db': 'exchange_db'
        },
        'bg_repeat_interval': 60
    }
    ExchangerBot(config).start()
